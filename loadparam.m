function [ p, opt, frame, SamplingScale, FrameRange, color_option, MaxBufSize ] = loadparam( title, str )
%
% p = [px, py, sx, sy, theta]; The location of the target in the first frame.
% px and py are th coordinates of the centre of the box
% sx and sy are the size of the box in the x (width) and y (height)
%   dimensions, before rotation
% theta is the rotation angle of the box
%
% 'opt.numsample',200,   The number of samples used in the condensation
% algorithm/particle filter.  Increasing this will likely improve the
% results, but make the tracker slower.
%
% 'opt.condenssig',0.01,  The standard deviation of the observation likelihood.
%
% 'opt.tmplsize', [32,32] The resolution at which the tracking window is
% sampled, in this case 32 pixels by 32 pixels.  If your initial
% window (given by p) is very large you may need to increase this.
%
% 'opt.affsig',[4,4,.02,.02,.005,.001]  These are the standard deviations of
% the dynamics distribution, that is how much we expect the target
% object might move from one frame to the next.  The meaning of each
% number is as follows:
%
%    affsig(1) = x translation (pixels, mean is 0)
%    affsig(2) = y translation (pixels, mean is 0)
%    affsig(3) = rotation angle (radians, mean is 0)
%    affsig(4) = x scaling (pixels, mean is 1) width
%    affsig(5) = y scaling (pixels, mean is 1) height
%    affsig(6) = scaling angle (radians, mean is 0)
%
% Rui.Yao, 28/06/2012.
%
% yaorui@mail.nwpu.edu.cn

%% Load Data
switch (title)
    case 'cubicle'; 
        p = [60 34 26 33 0]; 
        p(1)=p(1)+0.5*p(3); p(2)=p(2)+0.5*p(4);
    otherwise;  error(['unknown title ' title]);
end

%% Parameter setting
if ~exist('opt','var')  opt = [];  end
if ~isfield(opt,'condenssig') opt.condenssig = 0.01;  end

if ~isfield(opt,'maxbasis')   opt.maxbasis = 16;  end
if ~isfield(opt,'batchsize')  opt.batchsize = 2;  end
if ~isfield(opt,'errfunc')    opt.errfunc = 'L2';  end
if ~isfield(opt,'ff')         opt.ff = 1.0;  end
if ~isfield(opt,'minopt')
    opt.minopt = optimset; opt.minopt.MaxIter = 25; opt.minopt.Display='off';
end

switch (title)
    case 'cubicle';
        frame = imread(sprintf('%s%s%s%05d.jpg',str, title, '/imgs/img', 1)); 
        SamplingScale = 0.5; 
        opt.tmplsize = [48 48];
        opt.affsig = [7,7,.05,.002,.01,.001]; 
        opt.numsample = 400;  
        MaxBufSize = 500;  
        FrameRange = 1:51; 
        color_option = [1 0 0];
    otherwise;  error(['unknown title ' title]);        
end

end

