%% Learn a discriminant function for tracking
%
% Input Parameters:
% trainAll: the training data
% trainLabels: the labels
% w: SVM weight of previous learning iteration
% params: learning parameters
%
% Output Parameters: 
% output_w: new SVM weight after seeing current data
% seg_data: seg_data
%
% Rui.Yao, 25/06/2012
%
% yaorui@mail.nwpu.edu.cn

function [ output_w, seg_data ] = train_tracking_model( trainAll, trainLabels, w, params )

x_train = single(trainAll);
t_train = single(trainLabels);

% Encode data
[Xe, seg_data] = encode_data(x_train, params);

% Training
if isempty(w) ~= 0
    output_w = pwl_sgd(x_train, seg_data, t_train, params.k, params.niters, params.lambda,...
                params.use_pwl, params.init_iter, params.use_sqrt, params.class_1_weight);
else
    output_w = pwl_sgd(x_train, seg_data, t_train, params.k, params.niters, params.lambda,...
                params.use_pwl, params.init_iter, params.use_sqrt, params.class_1_weight, w);
end

end
