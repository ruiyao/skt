function CandSampSet = get_candidate_HOG(frame, n, ParticleCriteriaMinus, ParticleCriteriaPlus)

%% Feature description
Im = 256*frame; 
B = 9;
[L,C]=size(Im); % L num of lines ; C num of columns
m=sqrt(L/2);
if C==1
    Im=im_recover(Im,m,2*m);
    L=2*m;
    C=m;
end
Im=double(Im);

hx = [-1,0,1];
hy = -hx';
grad_xr = imfilter(double(Im),hx);
grad_yu = imfilter(double(Im),hy);
Tangles=atan2(grad_yu,grad_xr);
Tmagnit=((grad_yu.^2).*(grad_xr.^2)).^.5;
nwin_x=3; % set here the number of HOG windows per bound box
nwin_y=3;

%% Calc HOG
ParticleCriteriaMinus = single(ParticleCriteriaMinus);
ParticleCriteriaPlus = single(ParticleCriteriaPlus);
Tangles = single(Tangles);
Tmagnit = single(Tmagnit);

% tic;
CandSampSet = candidates(n, ParticleCriteriaMinus, ParticleCriteriaPlus,...
    Tangles, Tmagnit, nwin_x, nwin_y, B);
% n
% toc;

end

