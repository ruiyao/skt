%% Classify candidates patch for tracking
%
% Input Parameters:
% CandSampSet: candidates for testing
% params: learning parameters
% w: weight of SVM
% seg_data: seg_dat
% rho: scaling factor
%
% Output Parameters:
% val, idx: the SVM score and index of each candidate, respectively.
%
% Rui.Yao, 25/06/2012
%
% yaorui@mail.nwpu.edu.cn

function [ val,idx ] = classify_tracking_cands( CandSampSet, params, w, seg_data, rho )

CandSampSet = single(CandSampSet);

% tic;
% Encoding data
TestCands = encode_data(CandSampSet, params, seg_data);

% Testing
% start_test_time = cputime;
TestScore = w'*TestCands;

% Evaluating Likelihood
% likelihood = 1 ./ ( 1 + exp(-rho*TestScore)); % or likelihood = TestScore;
likelihood = TestScore;
[ val,idx ] = sort(likelihood, 'descend');

end

