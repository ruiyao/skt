// Calc HOG feature for candidates. Rui.Yao, 29/06/2012 

#include <string.h>
#include <math.h>
#include <mex.h>
#include <stdio.h>

// #define DEBUG_MODE
#define PI 3.1416

int g_verbosity = 1;

void vprint(const char *s) // wraps printing to control verbosity
{
	if (g_verbosity>0)
	{
		mexPrintf(s);
	}
}

// output matrix in matlab style
// NOTE: c style matrix concatenate the column of matlab style matrix to a vector.
void printMatrix(float* mat, int width, int height)
{
    for (int i = 0; i < width; i++)
        for (int j = 0; j < height; j++)
            printf("Mat(%d,%d) = %f\n", j+1, i+1, mat[i*height+j]);
    printf("\n");
}

void printMatrix(double* mat, int width, int height)
{
    for (int i = 0; i < width; i++)
        for (int j = 0; j < height; j++)
            printf("Mat(%d,%d) = %f\n", j+1, i+1, mat[i*height+j]);
    printf("\n");
}

void copyPartialMatrix(float* Dest, float* Source, int startRow, int startCol, int width, int height,
        int maxWidth, int maxHeight)
{
	for (int i = 0; i < width; i++)
    {
        for (int j = 0; j < height; j++)
        {
            Dest[i*height+j] = Source[ (startCol+i)*maxHeight + (startRow+j) ];
        }
    }
}

void computeHOG(double *H, float* angles, float* magnit, int nbin, int nwin_y, int nwin_x, 
        int step_y, int step_x, int width, int height)
{
    int cont=0;

    for (int n = 0; n <= nwin_y-1; n++)
    {
        for (int m = 0; m <= nwin_x-1; m++)
        {
            cont = cont + 1;
            // angles2 = angles(n*step_y+1:(n+2)*step_y,m*step_x+1:(m+2)*step_x);
            int hei = (n+2)*step_y - (n*step_y + 1) + 1;
            int wid = (m+2)*step_x - (m*step_x + 1) + 1;
            float* v_angles = (float *)malloc(sizeof(float)*hei*wid);
            copyPartialMatrix(v_angles, angles, n*step_y, m*step_x, wid, hei, width, height);
            //  printMatrix(v_angles, wid, hei);
            
            float* v_magnit = (float *)malloc(sizeof(float)*hei*wid);
            copyPartialMatrix(v_magnit, magnit, n*step_y, m*step_x, wid, hei, width, height);
            //  printMatrix(v_magnit, wid, hei);
            
            int K = hei*wid;
            // assembling the histogram with 9 bins (range of 20 degrees per bin)
            int bin = 0;
            double* H2 = (double *)malloc(sizeof(double)*nbin*1);
            // initilize
            for (int i = 0; i < nbin; i++)
            {
                H2[i] = 0;
            }
            double ang_lim = -PI+2*PI/nbin;
            while( ang_lim <= PI )
            {
                for (int k = 0; k < K; k++)
                {
                    if (v_angles[k] < ang_lim)
                    {
                        v_angles[k] = 100;
                        H2[bin] = H2[bin] + v_magnit[k];
                    }
                }
                //  printf("H2[%d] = %f\n", bin, H2[bin]);
                bin = bin+1;
                ang_lim += 2*PI/nbin;
            }
            
            int sum_H2 = 0;
            for (int i = 0; i < nbin; i++)
            {
                sum_H2 += H2[i];
            }
            
            // normalize
            for (int i = 0; i < nbin; i++)
            {
                H2[i] = H2[i]/(sum_H2+0.01);
                // printf("H2(%d) = %f\n", i, H2[i]); 
            }

            for (int i = (cont-1)*nbin, k = 0; i < cont*nbin; i++, k++)
            {
                H[i] = H2[k];
            }

            free(v_angles), free(v_magnit), free(H2);
            // printf("nwin_x: m = %d\n", m);
        }
        // printf("nwin_y: n = %d\n", n);
    }
    
    // for (int i = 0; i < m_dim; i++)
       //  printf("H(%d) = %f\n", i, H[i]);
}

void mexFunction (int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
	int  mlen = 4097;
	char* message = (char *)malloc(sizeof(char)*mlen);

	if (!message){ mexErrMsgTxt("Memories... I have none.\n"); return; }

	int err=0;
	if (nrhs!=8)
	{
		mexPrintf("Usage : \n");
		mexPrintf("  [ candidates ] = candidates( int(Samples( n )),\n");
		mexPrintf("                               double(ParticleCriteriaMinus( 2, n )),\n");
		mexPrintf("                               double(ParticleCriteriaPlus( 2, n )) ,\n");
		mexPrintf("                               double(Tangles( Im_height, Im_width )) ,\n");
		mexPrintf("                               double(Tmagnit( Im_height, Im_width ) ) ),\n");
		mexPrintf("                               int(nwin_x( nwin_x ) ),\n");
		mexPrintf("                               int(nwin_y( nwin_y ) ),\n");
		mexPrintf("                               int(Bin_num( B ) ),\n");
        mexPrintf(" your input had nlhs=%d  nrhs=%d.\n",nlhs,nrhs);

		return;
	}
    
	// n //////////////////////////////////////////////////////////////////////
	if ((!mxIsDouble(prhs[0]))|(mxIsComplex(prhs[0]))){
		err = 1;
		vprint("Samples number n should be of type (real) double\n");
		return;
	}
	int n = (int)*((double *)mxGetData(prhs[0]));

	// ParticleCriteriaMinus //////////////////////////////////////////////////
	if ((!mxIsSingle(prhs[1]))|(mxIsComplex(prhs[1])))
	{
		err = 1;
		vprint("ParticleCriteriaMinus should be of type (real) double\n");
		return;
	}
	const int *sz1 = NULL;
	int dummy_2  = 0;
    int dummy_ndims = 0;

	if (2!=mxGetNumberOfDimensions(prhs[1]))
	{
		err = 1;
		vprint("ParticleCriteriaMinus should be 2 dimensional\n");
		return;
	}
	else
	{
		sz1 = mxGetDimensions(prhs[1]);
		dummy_2  = sz1[0];
		dummy_ndims  = sz1[1];
	}

	if ((dummy_ndims!=n)|(dummy_2!=2))
    {
		err = 3;
		vprint("ParticleCriteriaMinus should be 2 x n \n");
		return;
	}
	float* ParticleCriteriaMinus = (float *)mxGetData(prhs[1]);
    
	// ParticleCriteriaPlus //////////////////////////////////////////////////
	if ((!mxIsSingle(prhs[2]))|(mxIsComplex(prhs[2])))
	{
		err = 1;
		vprint("ParticleCriteriaPlus should be of type (real) double\n");
		return;
	}
	const int *sz2 = NULL;
	dummy_2  = 0;
    dummy_ndims = 0;

	if (2!=mxGetNumberOfDimensions(prhs[2]))
	{
		err = 1;
		vprint("ParticleCriteriaPlus should be 2 dimensional\n");
		return;
	}
	else
	{
		sz2 = mxGetDimensions(prhs[2]);
		dummy_2  = sz2[0];
		dummy_ndims  = sz2[1];
	}

	if ((dummy_ndims!=n)|(dummy_2!=2)){
		err = 3;
		vprint("ParticleCriteriaPlus should be 2 x n \n");
		return;
	}
	float* ParticleCriteriaPlus = (float *)mxGetData(prhs[2]);

    // Tangles ////////////////////////////////////////////////////////////
	if ((!mxIsSingle(prhs[3]))|(mxIsComplex(prhs[3])))
	{
		err = 1;
		vprint("Tangles should be of type (real) double\n");
		return;
	}
	const int *sz3 = NULL;
	int width  = 0;
    int height = 0;

	if (2!=mxGetNumberOfDimensions(prhs[3]))
	{
		err = 1;
		vprint("ParticleCriteriaPlus should be 2 dimensional\n");
		return;
	}
	else
	{
		sz3 = mxGetDimensions(prhs[3]);
		height = sz3[0];
		width = sz3[1];
	}
	float* Tangles = (float *)mxGetData(prhs[3]);
    
	// Tmagnit ////////////////////////////////////////////////////////////
	if ((!mxIsSingle(prhs[4]))|(mxIsComplex(prhs[4])))
	{
		err = 1;
		vprint("Tmagnit shoucandidatesld be of type (real) double\n");
		return;
	}

    if (2!=mxGetNumberOfDimensions(prhs[4]))
	{
		err = 1;
		vprint("ParticleCriteriaPlus should be 2 dimensional\n");
		return;
	}
	float* Tmagnit = (float *)mxGetData(prhs[4]);

	// nwin_x //////////////////////////////////////////////////////////////
	if ((!mxIsDouble(prhs[5]))|(mxIsComplex(prhs[5])))
	{
		err = 1;
		vprint("nwin_x should be of type (real) double\n");
		return;
	}
	int nwin_x = (int)*(double *)mxGetData(prhs[5]);

	// nwin_y /////////////////////////////////////////////////////////////
	if ((!mxIsDouble(prhs[6]))|(mxIsComplex(prhs[6])))
	{
		err = 1;
		vprint("nwin_y should be of type (real) double\n");
		return;
	}
	int nwin_y = (int)*(double *)mxGetData(prhs[6]);

	// nbin ////////////////////////////////////////////////////////////////
	if ((!mxIsDouble(prhs[7]))|(mxIsComplex(prhs[7]))){
		err = 1;
		vprint("nbin should be of type (real) double\n");
		return;
	}
	int nbin = (int)*(double *)mxGetData(prhs[7]);

#ifdef DEBUG_MODE
    mexPrintf("=============================================\n");
    mexPrintf("|              Input Parameters             |\n");
    mexPrintf("=============================================\n");
    mexPrintf("                         n = %d\n",n);
	mexPrintf("dim(ParticleCriteriaMinus) = 2 x %d\n", n);
    mexPrintf(" dim(ParticleCriteriaPlus) = 2 x %d\n", n);    
    mexPrintf("           image_size(HxW) = %d x %d\n", height, width);
    mexPrintf("              dim(Tangles) = %d x %d\n", height, width); 
    mexPrintf("              dim(Tmagnit) = %d x %d\n", height, width); 
    mexPrintf("                    nwin_x = %d\n", nwin_x);
    mexPrintf("                    nwin_y = %d\n", nwin_y);
    mexPrintf("                      nbin = %d\n", nbin);
    mexPrintf("--------------------------------------------\n");
#endif
    
    int m = nwin_x*nwin_y*nbin*5;
    plhs[0] = mxCreateDoubleMatrix(0, 0, mxREAL); // Create an empty array 
    mxSetM(plhs[0], m); // Set the dimensions to M x N 
    mxSetN(plhs[0], n);
    mxSetData(plhs[0], mxMalloc(sizeof(double)*m*n)); // Allocate memory for the array 

    double *CandSampSet = (double *)mxGetData(plhs[0]);
    // double *CandSampSet = (double *)malloc(sizeof(double)*nwin_x*nwin_y*nbin*5*n);
    
	for (int i=0; i<n; i++)
	{
        float roi_1 = floor(ParticleCriteriaMinus[1+2*i]);
        float roi_2 = floor(ParticleCriteriaPlus[1+2*i]);
        float roi_3 = floor(ParticleCriteriaMinus[2*i]);
        float roi_4 = floor(ParticleCriteriaPlus[2*i]);
        
        // printf("roi_1 = %f, roi_2 = %f, roi_3 = %f, roi_4 = %f\n", roi_1,roi_2,roi_3,roi_4);
        if (roi_3 < 1)
        {
            roi_3 = 1;
        }

        if (roi_3 > width)
        {
            roi_3 = width;
        }

        if (roi_4 < 1)
        {
            roi_4 = 1;
        }

        if (roi_4 > width)
        {
            roi_4 = width;
        }

        if (roi_1 < 1)
        {
            roi_1 = 1;
        }

        if (roi_1 > height)
        {
            roi_1 = height;
        }

        if (roi_2 < 1)
        {
            roi_2 = 1;
        }

        if (roi_2 > height)
        {
            roi_2 = height;
        }
        
        roi_1 = roi_1 - 1; 
        roi_2 = roi_2 - 1;
        roi_3 = roi_3 - 1;
        roi_4 = roi_4 - 1;
        
        int hei = roi_2 - roi_1 + 1, wid = roi_4 - roi_3 + 1;
        double *tmpH = (double *)malloc(sizeof(double)*nwin_x*nwin_y*nbin*5);
        // angles = Tangles(roi(1):roi(2), roi(3):roi(4));
        // magnit = Tmagnit(roi(1):roi(2), roi(3):roi(4));
        // printMatrix(Tangles, width, height);
        // printMatrix(Tmagnit, width, height);
                
        int hei2 = hei, wid2 = wid;        
        float* angles = (float *)malloc(sizeof(float)*hei2*wid2);
        copyPartialMatrix( angles, Tangles, roi_1, roi_3, wid2, hei2, width, height );
        // printMatrix(angles, wid2, hei2);
        float* magnit = (float *)malloc(sizeof(float)*hei2*wid2);
        copyPartialMatrix( magnit, Tmagnit, roi_1, roi_3, wid2, hei2, width, height );
        // printMatrix(magnit, wid2, hei2);
        int step_x = floor((double)(wid2/(nwin_x+1)));
        int step_y = floor((double)(hei2/(nwin_y+1)));
        double* H_t = (double *)malloc(sizeof(double)*nwin_x*nwin_y*nbin);
        computeHOG(H_t, angles, magnit, nbin, nwin_y, nwin_x, step_y, step_x, wid2, hei2);
        // printMatrix(H_t, nwin_x*nwin_y*nbin, 1);

        memcpy(tmpH, H_t, sizeof(double)*nwin_x*nwin_y*nbin);
        free(angles), free(magnit), free(H_t);   
        
        // angles = Tangles(roi(1):roi(2), roi(3):floor(roi(4)-0.5*wid));
        hei2 = roi_2 - roi_1 + 1, wid2 = floor(roi_4-0.5*wid) - roi_3 + 1;
        angles = (float *)malloc(sizeof(float)*hei2*wid2);
        copyPartialMatrix( angles, Tangles, roi_1, roi_3, wid2, hei2, width, height );
        // printMatrix(angles, wid2, hei2);
        magnit = (float *)malloc(sizeof(float)*hei2*wid2);
        copyPartialMatrix( magnit, Tmagnit, roi_1, roi_3, wid2, hei2, width, height );
        // printMatrix(magnit, wid2, hei2);
        step_x = floor((double)(wid2/(nwin_x+1)));
        step_y = floor((double)(hei2/(nwin_y+1)));
        double* H_l = (double *)malloc(sizeof(double)*nwin_x*nwin_y*nbin); 
        computeHOG(H_l, angles, magnit, nbin, nwin_y, nwin_x, step_y, step_x, wid2, hei2);
        // printMatrix(H_l, nwin_x*nwin_y*nbin, 1);
        
        memcpy(tmpH + nwin_x*nwin_y*nbin, H_l, sizeof(double)*nwin_x*nwin_y*nbin);
        free(angles), free(magnit), free(H_l);  
        
        // angles = Tangles(roi(1):roi(2), floor(roi(3)+0.5*wid):roi(4));
        hei2 = roi_2 - roi_1 + 1, wid2 = roi_4 - floor(roi_3+0.5*wid) + 1;
        angles = (float *)malloc(sizeof(float)*hei2*wid2);
        copyPartialMatrix( angles, Tangles, roi_1, floor(roi_3+0.5*wid), wid2, hei2, width, height );
        // printMatrix(angles, wid2, hei2);
        magnit = (float *)malloc(sizeof(float)*hei2*wid2);
        copyPartialMatrix( magnit, Tmagnit, roi_1, floor(roi_3+0.5*wid), wid2, hei2, width, height );
        // printMatrix(magnit, wid2, hei2);
        step_x = floor((double)(wid2/(nwin_x+1)));
        step_y = floor((double)(hei2/(nwin_y+1)));
        double* H_r = (double *)malloc(sizeof(double)*nwin_x*nwin_y*nbin);
        computeHOG(H_r, angles, magnit, nbin, nwin_y, nwin_x, step_y, step_x, wid2, hei2);
        // printMatrix(H_r, nwin_x*nwin_y*nbin, 1);
        
        memcpy(tmpH + 2 *nwin_x*nwin_y*nbin, H_r, sizeof(double)*nwin_x*nwin_y*nbin);
        free(angles), free(magnit), free(H_r); 
        
        // angles = Tangles(roi(1):floor(roi(2)-0.5*hei), roi(3):roi(4));
        hei2 = floor(roi_2-0.5*hei) - roi_1 + 1, wid2 = roi_4 - roi_3 + 1;
        angles = (float *)malloc(sizeof(float)*hei2*wid2);
        copyPartialMatrix( angles, Tangles, roi_1, roi_3, wid2, hei2, width, height );
        // printMatrix(angles, wid2, hei2);
        magnit = (float *)malloc(sizeof(float)*hei2*wid2);
        copyPartialMatrix( magnit, Tmagnit, roi_1, roi_3, wid2, hei2, width, height );
        // printMatrix(magnit, wid2, hei2);
        step_x = floor((double)(wid2/(nwin_x+1)));
        step_y = floor((double)(hei2/(nwin_y+1)));
        double* H_u = (double *)malloc(sizeof(double)*nwin_x*nwin_y*nbin);
        computeHOG(H_u, angles, magnit, nbin, nwin_y, nwin_x, step_y, step_x, wid2, hei2); 
        // printMatrix(H_u, nwin_x*nwin_y*nbin, 1);

        memcpy(tmpH + 3 * nwin_x*nwin_y*nbin, H_u, sizeof(double)*nwin_x*nwin_y*nbin);
        free(angles), free(magnit), free(H_u);

        // angles = Tangles(floor(roi(1)+0.5*hei):roi(2), roi(3):roi(4));
        hei2 = roi_2 - floor(roi_1+0.5*hei) + 1, wid2 = roi_4 - roi_3 + 1;
        angles = (float *)malloc(sizeof(float)*hei2*wid2);
        copyPartialMatrix( angles, Tangles, floor(roi_1+0.5*hei), roi_3, wid2, hei2, width, height );
        // printMatrix(angles, wid2, hei2);
        magnit = (float *)malloc(sizeof(float)*hei2*wid2);
        copyPartialMatrix( magnit, Tmagnit, floor(roi_1+0.5*hei), roi_3, wid2, hei2, width, height );
        // printMatrix(magnit, wid2, hei2);
        step_x = floor((double)(wid2/(nwin_x+1)));
        step_y = floor((double)(hei2/(nwin_y+1)));
        double* H_d = (double *)malloc(sizeof(double)*nwin_x*nwin_y*nbin);
        computeHOG(H_d, angles, magnit, nbin, nwin_y, nwin_x, step_y, step_x, wid2, hei2);
        // printMatrix(H_d, nwin_x*nwin_y*nbin, 1);
        
        memcpy(tmpH + 4 * nwin_x*nwin_y*nbin, H_d, sizeof(double)*nwin_x*nwin_y*nbin);
        free(angles), free(magnit), free(H_d);
        
        // for (int k = 0; k < 5*nwin_x*nwin_y*nbin; k++)
        // printf("tmpH(%d) = %f\n", k, tmpH[k]);
	    
        memcpy(CandSampSet + i*nwin_x*nwin_y*nbin*5, tmpH, sizeof(double)*nwin_x*nwin_y*nbin*5);
        free(tmpH);
        // printf("CandSampSet: i = %d\n", i);
    }
    
    // printMatrix(CandSampSet, nwin_x*nwin_y*nbin*5*n, 1);
}

/*

Some useful functions from matrix.h
-----------------------------------

*** for 2d arrays ***

mxArray *mxCreateNumericMatrix(mwSize m, mwSize n, 
  mxClassID classid, mxComplexity ComplexFlag);

to get pointer to beginning of array (double *)
double *mxGetPr(const mxArray *pm);  

Get pointer to character array data
mxChar *mxGetChars(const mxArray *array_ptr);

Get pointer to array data
void *mxGetData(const mxArray *pm);

mxGetProperty returns the value at pa[index].propname
mxArray *mxGetProperty(const mxArray *pa, mwIndex index,
         const char *propname);

mxClassID mxGetClassID(const mxArray *pm);
const char *mxGetClassName(const mxArray *pm);

size_t mxGetM(const mxArray *pm);
size_t mxGetN(const mxArray *pm);


*** for nd array ***

mxArray *mxCreateNumericArray(mwSize ndim, const mwSize *dims, 
         mxClassID classid, mxComplexity ComplexFlag);

mwSize mxGetNumberOfDimensions(const mxArray *pm);

Use mxGetNumberOfDimensions to determine how many dimensions are in
the specified array. To determine how many elements are in each
dimension, call mxGetDimensions.


const mwSize *mxGetDimensions(const mxArray *pm);

The address of the first element in the dimensions array. Each integer
in the dimensions array represents the number of elements in a
particular dimension. The array is not NULL terminated.


*** for strings (in array single byte per char ) ***

int mxGetString(const mxArray *pm, char *str, mwSize strlen);


*** enums ***

typedef enum {
    mxREAL,
    mxCOMPLEX
} mxComplexity;


typedef enum {
	mxUNKNOWN_CLASS = 0,
	mxCELL_CLASS,
	mxSTRUCT_CLASS,
	mxLOGICAL_CLASS,
	mxCHAR_CLASS,
	mxVOID_CLASS,
	mxDOUBLE_CLASS,
	mxSINGLE_CLASS,
	mxINT8_CLASS,
	mxUINT8_CLASS,
	mxINT16_CLASS,
	mxUINT16_CLASS,
	mxINT32_CLASS,
	mxUINT32_CLASS,
	mxINT64_CLASS,
	mxUINT64_CLASS,
	mxFUNCTION_CLASS,
        mxOPAQUE_CLASS,
	mxOBJECT_CLASS
} mxClassID;


*/

