% Main function for Sparse Kernel Tracking. 
%
% Rui.Yao, 25/06/2012.
%
% ruiyao@cumt.edu.cn

function SparseKernelTracking
% clear all;
clc;   close all;

%% initialize variables
addpath 'pwl_sgd/'; % code for training piecewise linear models directly
addpath 'candidate_hog/'

rand('state',0); 
randn('state',0);

title = 'cubicle'; % cubicle % OneLeaveShopReenter2cor % lemming
% pathname = '../../Dataset/sequences/';
pathname = 'sequences/';

%% create folder
% if ~isdir([pathname, '/', title, '/skt_gt'])
%     mkdir([pathname, '/', title, '/SKT_gt']);
% end

%% Load data & parameters for tracking
[ p, opt, frame, SamplingScale, FrameRange, color_option, MaxBufSize ]...
    = loadparam(title, pathname);

OrgFrame = frame;
if ~isgray(frame)
    frame = rgb2gray(frame); % for color image, 3 channels
end
frame = double(frame)/256; 
plotlinewidth_option = 2.5;

param0 = [p(1), p(2), p(3)/opt.tmplsize(1), p(5), p(4)/p(3), 0];
param0 = affparam2mat(param0);

if size(frame,3)==1
    tmpl.mean = warpimg(frame, param0, opt.tmplsize);
else
    tmpl.mean = warpimg(rgb2gray(frame), param0, opt.tmplsize);
end
sz = size(tmpl.mean); 

%% set parameters for learning
learningParams.lambda         = 1e-4;
learningParams.C              = 1;
learningParams.ndivs          = 20;
learningParams.niters         = 100;
learningParams.k              = 10; %number of entries per iteration
learningParams.use_pwl        = 0;
learningParams.use_sqrt       = 1;
learningParams.class_1_weight = 1;
learningParams.init_iter      = 0;

%% Main body
param = [];
param.est = param0;
param.wimg = tmpl.mean;
if (exist('truepts','var'))
    npts = size(truepts,2);
    aff0 = affparaminv(param.est);
    pts0 = aff0([3,4,1;5,6,2]) * [truepts(:,:,1); ones(1,npts)];
    pts = cat(3, pts0 + repmat(sz'/2,[1,npts]), truepts(:,:,1));
else
    pts = [];
end

%% draw initial track window
drawopt = drawtrackresult([], FrameRange(1), OrgFrame, tmpl, param, pts, color_option, plotlinewidth_option);
disp('resize the window as necessary, then press any key..'); pause;
drawopt.showcondens = 0;  drawopt.thcondens = 1/opt.numsample;

% Labeled data
PosData = [];
NegData = [];
SegData = [];
PosNum = [];
NegNum = [];
w = [];

% track the sequence from frame 2 onward
duration = 0;
if (exist('dispstr','var'))  dispstr='';  end

TrackParam = {};
BufPool = [];

strFileName_gt = [pathname, '/', title, '/skt_test_', title, '_gt.txt'];
f_gt = fopen(strFileName_gt, 'w');

for f = FrameRange
    start_t = cputime;
    
    frame = imread(sprintf('%s%s%s%05d.jpg',pathname, title, '/imgs/img', f)); 
    OrgFrame = frame;
    if ~isgray(frame)
        frame = rgb2gray(frame);
    end
    frame = double(frame)/256;
    
    %% particle diffusion
    n = opt.numsample;
    param.param = repmat(affparam2geom(param.est(:)), [1,n]);
    RndGen = randn(6,n);
    param.param = param.param + RndGen.*repmat(opt.affsig(:),[1,n]);
    param.param(4,:) = 0; % for rotation
    
    % Paritcle corrections
    ParticleShape = [param.param(3,:).*sz(2); param.param(5,:).*param.param(3,:).*sz(2)];
    ParticleCriteriaPlus = param.param(1:2,:) + 0.5*ParticleShape;
    ParticleCriteriaMinus = param.param(1:2,:) - 0.5*ParticleShape;
    
    BufParam = param.param;
    Pidx1 = find(ParticleCriteriaPlus(1,:)>size(frame,2));
    if ~isempty(Pidx1)
        BufParam(1,Pidx1) = (size(frame,2) - 0.5*ParticleShape(1,Pidx1)); %
    end
    
    Pidx2 = find(ParticleCriteriaPlus(2,:)>size(frame,1));
    if ~isempty(Pidx2)
        BufParam(2,Pidx2) = (size(frame,1) - 0.5*ParticleShape(2,Pidx2));%
    end
    
    Midx1 = find(ParticleCriteriaMinus(1,:)<0);
    if ~isempty(Midx1)
        BufParam(1,Midx1) = 0.5*ParticleShape(1,Midx1); %
    end
    
    Midx2 = find(ParticleCriteriaMinus(2,:)<0);
    if ~isempty(Midx2)
        BufParam(2,Midx2) = 0.5*ParticleShape(2,Midx2); %
    end
    param.param = BufParam;
    ParticleShape = [param.param(3,:).*sz(2); param.param(5,:).*param.param(3,:).*sz(2)];
    ParticleCriteriaPlus = param.param(1:2,:) + 0.5*ParticleShape;
    ParticleCriteriaMinus = param.param(1:2,:) - 0.5*ParticleShape;

    %% Likelihood evaluation
    if f~=FrameRange(1)
        % Using particle filter to sample examples for testing, Rui.Yao, 15/06/2012
        CandSampSet = get_candidate_HOG(frame, n, ParticleCriteriaMinus, ParticleCriteriaPlus);

        rho = 0.05;
        [val,idx] = classify_tracking_cands(CandSampSet, learningParams, w, SegData, rho);
       
        param.est = affparam2mat(mean(param.param(:,idx(1)), 2));
    end
    
    %% training sample selection
    % positive sample generation
    CState = affparam2geom(param.est(:));
    CPShape = [CState(3).*sz(2); CState(5).*CState(3).*sz(2)];
    
    PosParam = repmat(CState, [1,9]);
    PosSig = [0.001*CPShape(1),0.001*CPShape(2),.0,.0,.0,.0]';
    SignMatrix = [0 1 -1 1 -1 0 0 1 -1 ; 0 1 -1 0 0 1 -1 -1 1];
    PosParam(1:2,:) = PosParam(1:2, :) + repmat(PosSig(1:2,:), [1 9]).*SignMatrix;
    
    PosParticleShape = [PosParam(3,:).*sz(2); PosParam(5,:).*PosParam(3,:).*sz(2)];
    PosParticleCriteriaPlus = PosParam(1:2,:) + 0.5*PosParticleShape;
    PosParticleCriteriaMinus = PosParam(1:2,:) - 0.5*PosParticleShape;
   
    % Examples for training, Rui.Yao, 15/06/2012
	PosCandSampSet = get_candidate_HOG(frame, size(PosParam, 2), PosParticleCriteriaMinus, PosParticleCriteriaPlus);
    
    PosData = [PosData PosCandSampSet];
    
    % negative sample selection
    NegParam = repmat(CState, [1,8]);
    NegSig = [SamplingScale*CPShape(1),SamplingScale*CPShape(2),.0,.0,.0,.0]'; %0.3  0.5
    SignMatrix = [1 -1 1 -1 0 0 1 -1 ; 1 -1 0 0 1 -1 -1 1];
    NegParam(1:2,:) = NegParam(1:2, :) + repmat(NegSig(1:2,:), [1 8]).*SignMatrix;
    
    NegParticleShape = [NegParam(3,:).*sz(2); NegParam(5,:).*NegParam(3,:).*sz(2)];
    NegParticleCriteriaPlus = NegParam(1:2,:) + 0.5*NegParticleShape;
    NegParticleCriteriaMinus = NegParam(1:2,:) - 0.5*NegParticleShape;
    
    % Hog feature extraction for negative samples
    % Examples for training, Rui.Yao, 15/06/2012
    NegCandSampSet = get_candidate_HOG(frame, size(NegParam, 2), NegParticleCriteriaMinus, NegParticleCriteriaPlus);
    NegData = [NegData NegCandSampSet];
    
    %% Sample buffer
    PosNum = size(PosData,2);
    NegNum = size(NegData,2);
    if PosNum>MaxBufSize
        PosZ = binornd(1, 0.8, 1, PosNum);
        PosData = PosData(:, find(PosZ==1));
    end
    if NegNum>MaxBufSize
        NegZ = binornd(1, 0.8, 1, NegNum);
        NegData = NegData(:, find(NegZ==1));
    end
    PosNum = size(PosData,2);
    NegNum = size(NegData,2);
    
    ClassLabels = [ones(PosNum, 1); -ones(NegNum, 1)]';
    
    learningParams.k = size(ClassLabels,2);
    
    %% PWL_SGD Updating
    [w, SegData] = train_tracking_model([PosData NegData], ClassLabels, w, learningParams);
    
    %% show results
    drawopt = drawtrackresult(drawopt, f, OrgFrame, tmpl, param, pts, color_option, plotlinewidth_option);
    
    elapsed_t = (cputime - start_t)/10;
    % fprintf('Frame %d: Elapsed time is %.3f seconds.', f, elapsed_t);
    duration = duration + elapsed_t;
    
    % save bounding box
    SKT_corners = max(round(ReturnAffineParamBoundingBox(opt.tmplsize, param.est)),1);
    bb_row = SKT_corners(1,1); 
    bb_col = SKT_corners(2,1);
    bb_width = SKT_corners(1,2) - SKT_corners(1,1);
    bb_height = SKT_corners(2,3) - SKT_corners(2,1);

    % save to files
    fprintf(f_gt, '%f,%f,%f,%f\n', bb_row, bb_col, bb_width, bb_height);
end
fclose(f_gt);

%% store results
fprintf('\n%d frames took %.3f seconds : %.3f fps\n', size(FrameRange, 2),...
    duration, size(FrameRange, 2)/duration);

end

